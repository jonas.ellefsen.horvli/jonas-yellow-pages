﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YellowPages
{
    class Contact
    {
        public string Name { get; private set; }
        public string LastName { get; private set; }
        public int PhoneNumber { get; set; }

        public Contact(string name, string lastName)
        {
            Name = name;
            LastName = lastName;
        }

        public Contact(string name, string lastName, int phoneNumber) : this(name, lastName)
        {
            PhoneNumber = phoneNumber;
        }

        public string GetFullName()
        {
            return Name + " " + LastName;
        }

        public bool Contains(string searchString)
        {
            if (GetFullName().ToLower().Contains(searchString.ToLower()))
            {
                return true;
            }
            return false;
        }

        public void PrintName()
        {
            Console.WriteLine(GetFullName());
        }

        public void PrintInfo()
        {
            PrintName();
            if (PhoneNumber != 0)
            {
                Console.WriteLine($"Phone number: {PhoneNumber}");
            }
        }

        public static List<Contact> findContacts(Contact[] contacts, string searchString)
        {
            List<Contact> contactList = new List<Contact>();

            foreach (Contact contact in contacts)
            {
                if (contact.Contains(searchString))
                {
                    contactList.Add(contact);
                }
            }

            return contactList;
        }
    }
}
