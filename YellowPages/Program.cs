﻿using System;
using System.Collections.Generic;

namespace YellowPages
{
    class Program
    {
        static void Main(string[] args)
        {
            Contact[] contacts = {
                new Contact("Jonas", "Horvli", 99091168),
                new Contact("Dean", "Schoultz"),
                new Contact("Greg", "Linklater"),
                new Contact("Cem", "Pedersen"),
                new Contact("Ole", "Baadshaug")
            };

            Console.WriteLine("Here are all the contacts:\n");
            foreach (Contact contact in contacts)
            {
                contact.PrintInfo();
                Console.WriteLine();
            }
            Console.Write("\n\n");
            while (true)
            {
                Console.WriteLine("\nEnter name to search contacts or press ctrl+c to exit:");
                string searchString = Console.ReadLine();
                List<Contact> contactList = Contact.findContacts(contacts, searchString);
                if (contactList.Count == 0)
                {
                    Console.WriteLine("Found no contacts..");
                }
                else
                {
                    Console.WriteLine("Contacts matching search:");
                    foreach (Contact contact in contactList)
                    {
                        contact.PrintInfo();
                        Console.WriteLine();
                    }
                }
            }


        }
    }
}
